import re


class Point:
    def __init__(self, x, y):
        self.__x = x
        self.__y = y

    @property
    def xy(self):
        return (self.__x, self.__y)

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    def __eq__(self, other):
        return self.__x == other.__x and self.__y == other.__y

    def __ne__(self, other):
        return self.__x != other.__x or self.__y != other.__y

    def __str__(self):
        return f"({self.__x}; {self.__y})"


class Figure:
    def __init__(self, pts):
        self.__product = None
        self.__l = None
        self.__v1 = None
        self.__v2 = None
        self._pts = pts
        self.__area = self._update_area()

    @property
    def area(self):
        return self.__area

    def __lt__(self, other):
        return self.__area < other.area

    def __le__(self, other):
        return self.__area <= other.area

    def __eq__(self, other):
        return self.__area == other.area

    def __ne__(self, other):
        return self.__area != other.area

    def __ge__(self, other):
        return self.__area >= other.area

    def __gt__(self, other):
        return self.__area > other.area

    def is_convex(self):
        self.__l = []
        for i in range(len(self._pts)):
            self.__v1 = Point(self._pts[0].x - self._pts[1].x, self._pts[0].y - self._pts[1].y)
            self.__v2 = Point(self._pts[2].x - self._pts[1].x, self._pts[2].y - self._pts[1].y)
            self.__product = self.__v1.x * self.__v2.y - self.__v1.y * self.__v2.x
            self.__l.append(self.__product)
        return all([x > 0 for x in self.__l]) or all([x < 0 for x in self.__l])


class Triangle(Figure):

    def _update_area(self):
        return 1 / 2 * abs(self._pts[0].x * self._pts[1].y
                           + self._pts[1].x * self._pts[2].y
                           + self._pts[2].x * self._pts[0].y
                           - self._pts[1].x * self._pts[0].y
                           - self._pts[2].x * self._pts[1].y
                           - self._pts[0].x * self._pts[2].y)

    def __str__(self):
        return f"[{self._pts[0]}; {self._pts[1]}; {self._pts[2]}]"


def split_pairs(line):
    n = 2
    new_list = []
    for i in range(0, len(line), n):
        element = line[i:i + n]
        new_list.append(element)

    return new_list


file = open('plist.txt', "r", encoding="utf-8").readline()
file = (re.sub('\W+', ' ', file)).split()

points = []
for i in range(0, len(file), 2):
    points.append(Point(int(file[i]), int(file[i + 1])))
triangles = []

for i in range(len(points) - 2):
    for j in range(i, len(points) - 1):
        for k in range(j, len(points)):
            t = Triangle((points[i], points[j], points[k]))
            if t.area != 0:
                triangles.append(t)

triangles = sorted(triangles)

if len(file) < 6:
    print("Слишком мало точек!")
print(f"Треугольник с max площадью: {triangles[-1]}, площадь: {triangles[-1].area}")
print(f"Треугольник с min площадью: {triangles[0]}, площадь: {triangles[0].area}")
