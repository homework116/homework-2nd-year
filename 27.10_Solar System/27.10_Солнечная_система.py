import pygame
import math
import random

pygame.init()
width = 1440
height = 800
screen = pygame.display.set_mode((width, height))
pygame.display.set_caption("Солнечная система")

FPS = 120
fpsClock = pygame.time.Clock()

# Палитра
black, white, gray, gray_orbit, orange, blue, red, pink, yellow, turquoise = (
    (0, 0, 0), (255, 255, 255),
    (79, 79, 79), (26, 26, 26),
    (201, 120, 44), (44, 126, 201),
    (201, 44, 44), (207, 103, 122),
    (217, 185, 69), (64, 166, 173))

# спрайты
planets_images = [
    pygame.image.load(f"{planet}.png") for planet in
    ["Sun", "Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"]
]
alien_image = pygame.image.load("alien.png")
belt_image = pygame.image.load("belt.png")

# легенда
legend_x = 10
legend_y = height - 740
legend_radius = 20
planet_colors = [gray, orange, blue, red, pink, yellow, turquoise, blue]
planet_names = {
    gray: "Меркурий 4880км 58млн км",
    orange: "Венера 12100км 108млн км",
    blue: "Земля 12742км 149,5млн км",
    red: "Марс 6779км 228млн км",
    pink: "Юпитер 139822км 778млн км",
    yellow: "Сатурн 116460км 1,426млрд км",
    turquoise: "Уран 50724км 4,496млрд км",
    blue: "Нептун 49244км 5,929млрд км"
}


def show_planet_name(planet_color, x, y):
    planet_name = planet_names.get(planet_color)
    font = pygame.font.Font(None, 24)
    text = font.render(planet_name, True, white)
    text_rect = text.get_rect()
    text_rect.topleft = (x + 2 * legend_radius, y - 10)
    screen.blit(text, text_rect)


legend_text = "Легенда: название планеты, ее размер, расстояние от Солнца до планеты"
font = pygame.font.Font(None, 24)
legend_text_surface = font.render(legend_text, True, white)
legend_text_rect = legend_text_surface.get_rect()
legend_text_rect.topleft = (15, 15)

# пояс
belt_width, belt_height = belt_image.get_size()
belt_x = (width - belt_width) / 2 - 25
belt_y = (height - belt_height) / 2 - 5

# звёзды
stars = []
for a_lot_of_star in range(300):
    x = random.randint(0, width)
    y = random.randint(0, height)
    stars.append((x, y))


# планеты + Солнце как планета
class Planet:
    def __init__(self, img, w, h, distance, speed):
        self.img = img
        self.w = w
        self.h = h
        self.distance = distance
        self.speed = speed
        self.angle = 0

    def draw(self):
        x = math.cos(self.angle) * self.distance + width / 2 - 80
        y = math.sin(self.angle) * self.distance + height / 2 - 60
        s = pygame.Rect(x + 35, y + 35, self.w, self.h)
        screen.blit(self.img, s)

    def move(self):
        self.angle = self.angle + self.speed

planets = [Planet(img, *params) for img, params in
           zip(planets_images, [(100, 100, -40, 0), (20, 20, 100, 47.36 / 10000),
                                (35, 35, 150, 35.02 / 10000), (40, 40, 200, 29.78 / 10000),
                                (24, 24, 250, 24.13 / 10000), (80, 80, 350, 13.07 / 10000),
                                (70, 70, 450, 9.69 / 10000), (50, 50, 550, 6.81 / 10000),
                                (45, 45, 650, 5.43 / 10000)])]

# запуск и отображение
running = True
while running:
    fpsClock.tick(FPS)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    screen.fill(black)

    for star in stars:
        pygame.draw.circle(screen, white, star, 1)

    screen.blit(belt_image, (belt_x, belt_y))

    for planet in planets:
        planet.draw()
        planet.move()

    for i, color in enumerate(planet_colors):
        x = legend_x + legend_radius
        y = legend_y + i * (2 * legend_radius + 15)
        planet_rect = pygame.Rect(x - legend_radius, y - legend_radius, 2 * legend_radius, 2 * legend_radius)
        pygame.draw.circle(screen, color, (x, y), legend_radius)

        if planet_rect.collidepoint(pygame.mouse.get_pos()):
            show_planet_name(color, x, y)
    screen.blit(legend_text_surface, legend_text_rect)

    screen.blit(alien_image, (width - 300, 0))

    pygame.display.update()

pygame.quit()
