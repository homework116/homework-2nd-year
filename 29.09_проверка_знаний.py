while True:
    choice = input("Введите номер задачи (от 1 до 16) чтобы решить её:")

    # 1
    if choice == "1":
        print("Выбрана задача 1!")

        a, b, c = map(int, input("Введите значения для переменных a, b и c через пробел: ").split())
        print("Ответ:", "a =", b, "b =", c, "c =", a)

    # 2.1 - пока что...
    elif choice == "2":
        print("Выбрана задача 2!")

        while True:
            try:
                num_1, num_2 = map(float, input("Введите два числа через пробел: ").split())
                break
            except ValueError:
                print("Ошибка! Возможно вы ввели не число...")
        print("Сумма введенных чисел:", num_1 + num_2)

    # 3.1 и 3.2
    elif choice == "3":
        print("Выбрана задача 3!")

        num = float(input("Введите число (от 0 до 100): "))

        if 0 <= num <= 100:
            result = 1
            for a in range(5):
                result = result * num
            print(f"{num} в пятой степени равно: {result}")
        else:
            print("Ошибка! Возможно вы ввели число не из заданного диапазона...")

    # 4
    elif choice == "4":
        print("Выбрана задача 4!")

        num_fib = float(input("Введите число (от 0 до 250): "))

        if 0 <= num_fib <= 250:
            fibonacci_numbers = {0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233}
            if num_fib in fibonacci_numbers:
                print(f"{num_fib} является числом Фибоначчи")
            else:
                print(f"{num_fib} НЕ является числом Фибоначчи, попробуйте другое число")
        else:
            print("Ошибка! Возможно вы ввели число не из заданного диапазона...")

    # 5
    elif choice == "5":
        print("Выбрана задача 5!")
        month = input("Введите месяц года в виде числа (от 1 до 12): ")

        if month in ["1", "2", "12"]:
            season = "зима"
        elif month in ["3", "4", "5"]:
            season = "весна"
        elif month in ["6", "7", "8"]:
            season = "лето"
        elif month in ["9", "10", "11"]:
            season = "осень"
        else:
            season = "...ошибки! Возможно вы выдумали несуществующий месяц.."
        print(f"{month} месяц  относится к сезону - {season}.")

    #5 второй вариант решения
    elif choice == "55":
        print("Выбрана задача 5! (второй вариант решения)")
        month = int(input("Введите месяц года в виде числа (от 1 до 12): "))

        if 1 <= month <= 12:
            # Словарь, связывающий номер месяца с временем года
            season_dict = {12: "зима", 1: "зима", 2: "зима",
                           3: "весна", 4: "весна", 5: "весна",
                           6: "лето", 7: "лето", 8: "лето",
                           9: "осень", 10: "осень", 11: "осень"}

            # Определение времени года с использованием словаря
            season = season_dict[month]
            print(f"{month} месяц относится к сезону - {season}.")
        else:
            print("Ошибка! Возможно вы ввели число не из заданного диапазона...")

    # 6
    elif choice == "6":
        print("Выбрана задача 6!")
        N_6 = int(input("Введите положительное число N : "))

        if N_6 < 1:
            print("Ошибка! Вы ввели число < 1")
        else:
            total = 0
            chet_count = 0
            ne_chet_count = 0

            for number in range(1, N_6 + 1):
                total += number
                if number % 2 == 0:
                    chet_count += 1
                else:
                    ne_chet_count += 1

            print(f"Сумма чисел от 1 до {N_6}: {total}")
            print(f"Количество четных чисел: {chet_count}")
            print(f"Количество нечетных чисел: {ne_chet_count}")

    # 7
    elif choice == "7":
        print("Выбрана задача 7!")

        N_7 = int(input("Введите число N (N < 250): "))

        if N_7 >= 250:
            print("N должно быть меньше 250")
        else:
            def count_divisors(num):
                count = 0
                for i in range(1, num + 1):
                    if num % i == 0:
                        count += 1
                return count

            # количество делителей
            for i in range(1, N_7 + 1):
                divisors_count = count_divisors(i)
                print(f"{i} {divisors_count}")

    # 8
    elif choice == "8":
        print("Выбрана задача 8!")

        n_num_1 = int(input("Введите начальное значение: "))
        m_num_2 = int(input("Введите конечное значение: "))
        flag = False
        for first_troika in range(n_num_1, m_num_2 + 1):
            for second_troika in range(first_troika + 1, m_num_2 + 1):
                for third_troika in range(n_num_1, m_num_2 + 1):
                    if first_troika ** 2 + second_troika ** 2 == third_troika ** 2:
                        print("Пифагоровы тройки в данном интервале:", first_troika, second_troika, third_troika)
                        flag = True
        if flag == False:
            print("В данном интервале нет троек")

    # 9
    elif choice == "9":
        print("Выбрана задача 9!")

        N_num_9 = int(input("Введите начальное значение: "))
        M_num_9 = int(input("Введите конечное значение: "))

        result = []

        for i in range(N_num_9, M_num_9 + 1):
            # digit - каждая цифра числа (if ни одна цифра не равна нулю, число делится на каждую свою цифру)
            if all(int(digit) != 0 and i % int(digit) == 0 for digit in str(i)):
                result.append(i)

        print(f"Числа в интервале от {N_num_9} до {M_num_9}, которые делятся на каждую из своих цифр:", result)

    # 10
    elif choice == "10":
        print("Выбрана задача 10!")

        kolvo_sover = int(input("Введите количество совершенных чисел (N < 5): "))

        if kolvo_sover >= 5:
            print("N должно быть меньше 5")
        else:

            perfect_num = []
            num = 1

            while len(perfect_num) < kolvo_sover:
                div_sum = sum(i for i in range(1, num) if num % i == 0)
                if div_sum == num:
                    perfect_num.append(num)
                num += 1

            print(f"Первые {kolvo_sover} совершенных чисел:")
            print(perfect_num)

    # 11
    elif choice == "11":
        print("Выбрана задача 11!")

        mas_input = input("Создайте свой массив! Введите элементы массива через пробел: ")
        massiv = list(map(int, mas_input.split()))
        print("Ваш массив: ", massiv)

        last_element_1 = massiv[-1]
        last_element_2 = massiv[len(massiv) - 1]
        last_element_3 = massiv.pop()

        print("Последний элемент массива:", "[-1] -->", last_element_1, "massiv[len(massiv) - 1] -->", last_element_2,
              "pop() -->", last_element_3)

    # 12
    elif choice == "12":
        print("Выбрана задача 12!")

        mas_input_12 = input("Создайте свой массив! Введите элементы массива через пробел: ")
        massiv_12 = list(map(int, mas_input_12.split()))
        print("Ваш массив: ", massiv_12)

        # Выводим массив в обратном порядке
        perevorot = massiv_12[::-1]

        print("Массив-перевёртыш:", perevorot)

    # 13 - её нет

    # 14
    elif choice == "14":
        print("Выбрана задача 14!")

        import tkinter as tk


        # Функция для конвертации рублей в доллары
        def v_dollars():
            try:
                rubles = float(entry.get())
                exchange_rate = 0.010391
                dollars = rubles * exchange_rate

                result_label.config(text=f"{rubles} рублей = {dollars:.2f} долларов")

            except ValueError:
                result_label.config(text="Введите число!")


        def v_rubles():
            try:
                dollars_2 = float(entry.get())
                exchange_rate = 96.24
                rubles = dollars_2 * exchange_rate

                result_label_two.config(text=f"{dollars_2} долларов = {rubles:.2f} рублей")

            except ValueError:
                result_label_two.config(text="Введите число!")


        app = tk.Tk()
        app.title("Конвертер рублей в доллары и наоборот")

        # рубли в доллар
        entry_label = tk.Label(app, text="Введите сумму в рублях:")
        entry_label.grid(row=0, column=0, padx=5, pady=5)

        entry = tk.Entry(app)
        entry.grid(row=0, column=1, padx=5, pady=5)

        convert_button = tk.Button(app, text="Конвертировать", command=v_dollars)
        convert_button.grid(row=0, column=3, padx=5, pady=5)

        result_label = tk.Label(app, text="")
        result_label.grid(row=0, column=2, padx=5, pady=5)
        #

        # доллар в рубли
        entry_label = tk.Label(app, text="Введите сумму в долларах:")
        entry_label.grid(row=3, column=0, padx=5, pady=5)

        entry_two = tk.Entry(app)
        entry_two.grid(row=3, column=1, padx=5, pady=5)

        convert_button_two = tk.Button(app, text="Конвертировать", command=v_rubles)
        convert_button_two.grid(row=3, column=3, padx=5, pady=5)

        result_label_two = tk.Label(app, text="")
        result_label_two.grid(row=3, column=2, padx=5, pady=5)
        #
        app.mainloop()

    # 15
    elif choice == "15":
        print("Выбрана задача 15!")

        try:
            stroki = int(input("Введите количество строк (от 5 до 20): "))
            stolbi = int(input("Введите количество столбцов (от 5 до 20): "))

            if 5 <= stroki <= 20 and 5 <= stolbi <= 20:
                for i in range(1, stroki + 1):
                    for j in range(1, stolbi + 1):
                        print(f"{i * j}", end="\t")
                    print()
            else:
                print("Неправильные размеры таблицы!")
        except ValueError:
            print("Введите целые числа")

    else:
        print("Задачи под данным номером не существует или она ещё не готова, введите другой номер.")
