import pygame
import sys
import random

pygame.init()
pygame.mixer.init()

# спрайты и украшения
SCREEN_WIDTH = 400
SCREEN_HEIGHT = 800
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Волк и Яйца")

WHITE = (255, 255, 255)

WOLF_SIZE = (75, 150)
EGG_SIZE = (30, 30)
HEALTH_SIZE = (33, 33)

font = pygame.font.Font(None, 36)

pygame.mixer.music.load("music.mp3")
pygame.mixer.music.play(-1)

background = pygame.image.load("background.png")
background = pygame.transform.scale(background, (SCREEN_WIDTH, SCREEN_HEIGHT))

game_over_background = pygame.image.load("game_over_background.png")
game_over_background = pygame.transform.scale(game_over_background, (SCREEN_WIDTH, SCREEN_HEIGHT))

egg_score_image = pygame.image.load("egg_score.png")
egg_score_image = pygame.transform.scale(egg_score_image, (30, 30))

health_image = pygame.image.load("health.png")
health_image = pygame.transform.scale(health_image, HEALTH_SIZE)

health_off_image = pygame.image.load("health_off.png")
health_off_image = pygame.transform.scale(health_off_image, HEALTH_SIZE)

chicken_image = pygame.image.load("chicken.png")
chicken_image = pygame.transform.scale(chicken_image, (400, 170))

restart_button = pygame.image.load("restart_button.png")
restart_button = pygame.transform.scale(restart_button, (200, 150))

# местоположение деталей и прочая информация
game_over = False
score = 0
spawn_positions = [75, 150, 225, 300]

chicken_rect = chicken_image.get_rect()
chicken_rect.x = 0
chicken_rect.y = 80

restart_button_rect = restart_button.get_rect()
restart_button_rect.center = (SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2 + 100)


# классы
class Wolf(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("wolf.png")
        self.image = pygame.transform.scale(self.image, WOLF_SIZE)
        self.rect = self.image.get_rect()
        self.rect.center = (SCREEN_WIDTH // 2, SCREEN_HEIGHT - 80)
        self.health = 3
        self.eggs_destroyed = 0

    def update(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT] and self.rect.left > 0:
            self.rect.x -= 5
        if keys[pygame.K_RIGHT] and self.rect.right < SCREEN_WIDTH:
            self.rect.x += 5


class Egg(pygame.sprite.Sprite):
    def __init__(self, x):
        super().__init__()
        self.is_golden = random.choice([False, False, False, True])
        if self.is_golden:
            self.image = pygame.image.load("golden_egg.png")
        else:
            self.image = pygame.image.load("egg.png")
        self.image = pygame.transform.scale(self.image, (30, 30))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = 150
        self.fallen = False
        self.broken_image = pygame.image.load("broken_egg.png")
        self.broken_image = pygame.transform.scale(self.broken_image, (30, 30))

    def update(self):
        if not self.fallen:
            self.rect.y += 3
            if self.rect.y > 750:
                self.image = self.broken_image
                self.rect.y = 750
                self.fallen = True


class Bomb(pygame.sprite.Sprite):
    def __init__(self, x):
        super().__init__()
        self.image = pygame.image.load("bomba.png")
        self.image = pygame.transform.scale(self.image, (30, 30))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = 150

    def update(self):
        self.rect.y += 3
        if self.rect.y > 750:
            self.kill()


def restart_game():
    global wolf, all_sprites, eggs, bombs, game_over, score
    wolf = Wolf()
    all_sprites = pygame.sprite.Group()
    eggs = pygame.sprite.Group()
    bombs = pygame.sprite.Group()
    all_sprites.add(wolf)
    game_over = False
    score = 0


# группы спрайтов
all_sprites = pygame.sprite.Group()
eggs = pygame.sprite.Group()
bombs = pygame.sprite.Group()
wolf = Wolf()
all_sprites.add(wolf)

clock = pygame.time.Clock()
spawn_timer = pygame.time.get_ticks()

# запуск игры
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.MOUSEBUTTONDOWN and game_over:
            if restart_button_rect.collidepoint(event.pos):
                restart_game()
    if not game_over:
        screen.blit(background, (0, 0))
        if pygame.time.get_ticks() - spawn_timer > 1000:
            egg_x = random.choice(spawn_positions)
            egg = Egg(egg_x)
            all_sprites.add(egg)
            eggs.add(egg)
            if random.randint(1, 20) == 1:
                bomb_x = random.choice(spawn_positions)
                bomb = Bomb(bomb_x)
                all_sprites.add(bomb)
                bombs.add(bomb)

            spawn_timer = pygame.time.get_ticks()

        collisions_eggs = pygame.sprite.spritecollide(wolf, eggs, True)
        collisions_bombs = pygame.sprite.spritecollide(wolf, bombs, True)

        for collision in collisions_eggs:
            if collision.is_golden and not collision.fallen:
                score += 5
            elif not collision.is_golden and not collision.fallen:
                score += 1
                wolf.eggs_destroyed += 1
                collision.fallen = True
        for collision in collisions_bombs:
            wolf.health -= 1
            score -= 50
            if wolf.health <= 0:
                game_over = True

        all_sprites.update()
        all_sprites.draw(screen)

        screen.blit(chicken_image, chicken_rect)

        screen.blit(egg_score_image, (10, 10))
        score_text = font.render("Счёт: {}".format(score), True, WHITE)
        screen.blit(score_text, (45, 18))

        for i in range(wolf.health):
            screen.blit(health_image, (SCREEN_WIDTH - 40 - i * 40, 10))
        for i in range(3 - wolf.health):
            screen.blit(health_off_image, (SCREEN_WIDTH - 40 - (wolf.health + i) * 40, 10))
    else:
        screen.blit(game_over_background, (0, 0))
        game_over_text = font.render("Вы проиграли!", True, WHITE)
        screen.blit(game_over_text, (SCREEN_WIDTH // 2 - game_over_text.get_width() // 2, SCREEN_HEIGHT // 2 - 150))
        score_text = font.render("Ваш счет: {}".format(score), True, WHITE)
        screen.blit(score_text, (SCREEN_WIDTH // 2 - score_text.get_width() // 2, SCREEN_HEIGHT // 2 - 115))
        screen.blit(restart_button, restart_button_rect)
    pygame.display.flip()

    clock.tick(30)
