class My_list(list):
    def __init__(self, *args):
        super().__init__(args)

    # вычитание
    def __sub__(self, other):
        result = My_list()
        for item1, item2 in zip(self, other):
            result.append(item1 - item2)
        return result

    # деление
    def __truediv__(self, divisor):
        result = My_list()
        for item in self:
            result.append(item / divisor)
        return result

    def __str__(self):
        return f"My_list changes: {list.__str__(self)}"

# выражение из форлабса
l1 = My_list(1, 2, 3)
l2 = My_list(3, 4, 5)
l3 = l2 - l1
l4 = l1 - l2
print(l1 / 5)
print(l3)
print(l4)
