from threading import Timer


class Cat:
    def __init__(self, hunger_points=10, happiness_points=5):
        self.__hunger = hunger_points
        self.__happiness = happiness_points
        self.__hunger_timer = Timer(5, self.make_hungry)
        self.__sadness_timer = Timer(10, self.make_sad)

    def get_hunger(self):
        return self.__hunger

    def get_happiness(self):
        return self.__happiness

    def set_hunger(self, value):
        self.__hunger = value

    def set_happiness(self, value):
        self.__happiness = value

    def feed(self):
        self.set_hunger(self.get_hunger() + 1)
        print(f"\U0001f63a -- \U0001f41f\U0001f357 Чавк-чавк, как вкусно! Голод: {self.get_hunger()}/10, , Счастье: {self.get_happiness()}/5")

    def play(self):
        self.set_happiness(self.get_happiness() + 2)
        print(f"\U0001f63a -- \U0001f9f6\U0001f94e Мне так весело с тобой! Голод: {self.get_hunger()}/10, Счастье: {self.get_happiness()}/5")
        self.set_hunger(self.get_hunger() - 1)

    def update(self):
        self.make_hungry()
        self.make_sad()

    def make_hungry(self):
        self.set_hunger(self.get_hunger() - 1)
        print(f"\U0001f63f Я проголодался... Голод: {self.get_hunger()}/10, Счастье: {self.get_happiness()}/5")
        if self.get_hunger() == 0:
            self.game_over("\U0001f480\U0001f9b4 котик умер с голоду!")
        else:
            self.__hunger_timer = Timer(10, self.make_hungry)
            self.__hunger_timer.start()

    def make_sad(self):
        self.set_happiness(self.get_happiness() - 1)
        print(f"\U0001f63f Мне так грустно... Голод: {self.get_hunger()}/10, Счастье: {self.get_happiness()}/5")
        if self.get_happiness() == 0:
            self.game_over("\U0001f408\U0001f4a8 котику было очень одиноко и он ушёл!")
        elif self.get_hunger() == 0:
            self.game_over("\U0001f480\U0001f9b4 котик умер с голоду!")
        else:
            self.__sadness_timer = Timer(20, self.make_sad)
            self.__sadness_timer.start()

    def game_over(self, reason):
        print(f"Конец игры: {reason}")
        self.stop()

    def start(self):
        self.__hunger_timer.start()
        self.__sadness_timer.start()

    def stop(self):
        self.__hunger_timer.cancel()
        self.__sadness_timer.cancel()


tamagotchi = Cat()
tamagotchi.start()

print("Ваш питомец:  \n\U0001f431 -- МААУ!")
print("Взаимодействие с питомцем (F - покормить, G - поиграть, X - выйти из игры):")
while True:
    command = input()

    if command.upper() == 'F':
        tamagotchi.feed()
    elif command.upper() == 'G':
        tamagotchi.play()
    elif command.upper() == 'X':
        tamagotchi.stop()
        break
    else:
        print("Введите правильную команду для взаимодействия с питомцем!")
